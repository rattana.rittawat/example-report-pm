package com.taywin.myapplication.modules

import com.taywin.myapplication.core.interactorusecase.usecaseposts.UseCasePostsImp
import com.taywin.myapplication.core.repository.PostRepository
import com.taywin.myapplication.core.repository.PostRepositoryImpl
import com.taywin.myapplication.datasource.PostRestDataStore
import com.taywin.myapplication.ui.features.exampleposts.MainViewModel
import com.taywin.myapplication.ui.features.login.LoginViewModel
import com.taywin.myapplication.ui.features.report.SearchReportViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private val dataModules = module {
    //region Interactor
    single { UseCasePostsImp(get()) }

    //endregion

    //region Repository
    single<PostRepository> { PostRepositoryImpl( get() )}
    //endregion

    //region Datastore
    single { PostRestDataStore() }
}

private val viewModel = module { 
    viewModel { MainViewModel( get() ) }
    viewModel { LoginViewModel( ) }
    viewModel { SearchReportViewModel( ) }
}

val mvvmModule
    = listOf(
        viewModel,
        dataModules
    )
