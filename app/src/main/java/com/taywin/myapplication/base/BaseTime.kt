package com.taywin.myapplication.base

import android.icu.util.Calendar
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.taywin.myapplication.utils.Global
import org.koin.core.component.KoinComponent

class BaseTime : ViewModel(), KoinComponent {

    private var year = -1
    private var currentMountIndex = -1
    private var moveIndex = 0

    val mount = MutableLiveData<String>()
    val forwardEnable = MutableLiveData<Boolean>()
    val backwardEnable = MutableLiveData<Boolean>()

    fun setup(){
        forwardEnable.value = false
        val now = Calendar.getInstance()
        year = now.get(Calendar.YEAR)
        currentMountIndex = now.get(Calendar.MONTH)
        mount.value = "${Global.MOUNT_NAME[currentMountIndex]} $year"
    }

    open fun onNextMountClick(){
        if (getMonthIndex() == 11){
            ++year
        }
        ++moveIndex
        updateMount()
    }

    open fun onPreviousMountClick(){
        if (getMonthIndex() == 0){
            --year
        }
        --moveIndex
        updateMount()
    }

    fun getMonthIndex() = if(currentMountIndex + moveIndex < 0) currentMountIndex + moveIndex + 12 else (currentMountIndex + moveIndex) % 12
    fun getYear() = year
    fun isCurrentMonth() = moveIndex == 0

    private fun updateMount(){
        mount.value = "${Global.MOUNT_NAME[getMonthIndex()]} $year"
        when {
            moveIndex == 0 -> {
                forwardEnable.value = false
                backwardEnable.value = true
            }
            moveIndex <= -2 -> {
                forwardEnable.value = true
                backwardEnable.value = false
            }
            else -> {
                forwardEnable.value = true
                backwardEnable.value = true
            }
        }
    }
}