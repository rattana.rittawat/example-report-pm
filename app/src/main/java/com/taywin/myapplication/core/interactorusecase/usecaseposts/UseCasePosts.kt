package com.taywin.myapplication.core.interactorusecase.usecaseposts

import com.taywin.myapplication.datasource.model.PostResponseData
import kotlinx.coroutines.flow.Flow

interface UseCasePosts {
    suspend fun executePost() : Flow<PostResponseData>
}