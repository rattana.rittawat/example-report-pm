package com.taywin.myapplication.core.repository

import com.taywin.myapplication.datasource.model.PostResponseData
import kotlinx.coroutines.flow.Flow

interface PostRepository {
    fun getPosts(): Flow<PostResponseData>
}