package com.taywin.myapplication.core.interactorusecase.usecaseposts

import com.taywin.myapplication.core.repository.PostRepository
import com.taywin.myapplication.datasource.model.PostResponseData
import kotlinx.coroutines.flow.Flow

class UseCasePostsImp(private val postRepository: PostRepository) : UseCasePosts {
    override suspend fun executePost(): Flow<PostResponseData> {
        return postRepository.getPosts()
    }
}