package com.taywin.myapplication.core.repository

import com.taywin.myapplication.datasource.PostRestDataStore
import com.taywin.myapplication.datasource.model.PostResponseData
import kotlinx.coroutines.flow.Flow

class PostRepositoryImpl(
    private val postRestDataStore: PostRestDataStore
) : PostRepository {

    override fun getPosts(): Flow<PostResponseData> =
        postRestDataStore.getPosts()

}