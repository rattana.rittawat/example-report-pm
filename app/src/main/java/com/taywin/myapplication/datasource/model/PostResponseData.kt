package com.taywin.myapplication.datasource.model

data class PostResponseData(
    val userId: Int?,
    val id: Int?,
    val title: String?,
    val body: String?
)