package com.taywin.myapplication.datasource

import com.taywin.myapplication.datasource.model.PostResponseData
import com.taywin.myapplication.datasource.service.PostApiClient
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class PostRestDataStore {

    fun getPosts(): Flow<PostResponseData> = flow {
        PostApiClient.getApiClient().getPosts().forEach {
            emit(it)
        }
    }

}