package com.taywin.myapplication.datasource.service

data class ResultResponse<T>(val status: Status, val data: T?, val message: String?){
    companion object{
        fun <T> success(data: T) = ResultResponse(Status.SUCCESS, data, null)
        fun <T> failure(msg: String) = ResultResponse<T>(Status.FAILURE, null, msg)
        fun <T> networkError() = ResultResponse<T>(Status.NETWORK_ERROR, null, null)
    }

    enum class Status {
        SUCCESS, FAILURE, NETWORK_ERROR
    }
}