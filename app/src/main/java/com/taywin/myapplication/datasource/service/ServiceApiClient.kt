package com.taywin.myapplication.datasource.service

import com.taywin.myapplication.datasource.model.PostResponseData
import retrofit2.http.GET
import retrofit2.http.Path

interface ServiceApiClient {
    @GET("/posts")
    suspend fun getPosts(): List<PostResponseData>

    @GET("/posts/{id}")
    suspend fun getPostById(@Path("id") id:String): PostResponseData
}