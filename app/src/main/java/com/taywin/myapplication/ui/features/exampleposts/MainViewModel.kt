package com.taywin.myapplication.ui.features.exampleposts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.taywin.myapplication.core.interactorusecase.usecaseposts.UseCasePostsImp
import com.taywin.myapplication.core.io
import com.taywin.myapplication.core.ui
import com.taywin.myapplication.ui.features.exampleposts.state.StateDataPost
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect
import org.koin.core.component.KoinComponent

class MainViewModel(
    private val getPostsInteractor: UseCasePostsImp
) : ViewModel(), KoinComponent {

    val viewState: LiveData<StateDataPost> get() = mViewState
    private val mViewState = MutableLiveData<StateDataPost>()

    //private val mPostVMMapper by lazy { PostVMMapper() }

    fun getPosts() {
        viewModelScope.launch {
            mViewState.value = StateDataPost.ShowLoader(true)
            try {
                io {
                    getPostsInteractor.executePost()
                        .collect {
                            ui { mViewState.value = StateDataPost.Post(it) }
                        }
                }
            } catch (e: Exception) {
                ui { mViewState.value = StateDataPost.Error(e.message) }
            }
            mViewState.value = StateDataPost.ShowLoader(false)
        }
    }

}