package com.taywin.myapplication.ui.features.exampleposts

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.taywin.myapplication.R
import com.taywin.myapplication.ui.features.exampleposts.state.StateDataPost
import kotlinx.android.synthetic.main.main_fragment.*
import org.koin.android.ext.android.get

class MainFragment : Fragment(), MainAdapter.Listener {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainViewModel = get()
    private var mAdapter = MainAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        mAdapter.setListener(this)

        rvPost.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = mAdapter
        }

        viewModel.getPosts()
    }

    private fun observeViewModel() {
//        TODO("Not yet implemented")
        viewModel.viewState.observe(viewLifecycleOwner, {
            when (it) {
                is StateDataPost.Post -> {
                    mAdapter.add(it.postVM)
                }
                is StateDataPost.ShowLoader ->{
                    if(it.showLoader){
                        pbPosts.visibility = View.VISIBLE
                        rvPost.visibility = View.GONE
                    }else{
                        pbPosts.visibility = View.GONE
                        rvPost.visibility = View.VISIBLE
                    }
                }
                is StateDataPost.Error -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }

        })
    }

    override fun onPostClicked(id: Int) {
//        TODO("Not yet implemented")
    }

}