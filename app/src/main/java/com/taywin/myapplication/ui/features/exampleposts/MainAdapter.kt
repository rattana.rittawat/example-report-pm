package com.taywin.myapplication.ui.features.exampleposts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.taywin.myapplication.R
import com.taywin.myapplication.datasource.model.PostResponseData
import kotlinx.android.synthetic.main.post_item.view.*

class MainAdapter : RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    private val mList: MutableList<PostResponseData> = mutableListOf()
    private var mListener: Listener? = null

    fun add(lstPostVM: PostResponseData) {
        mList.add(lstPostVM)
        notifyItemInserted(this.itemCount)
    }

    fun setListener(listener: Listener) {
        mListener = listener
    }

    interface Listener {
        fun onPostClicked(id: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.post_item, parent, false)
    )

    override fun getItemCount() = mList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mList[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(postVM: PostResponseData) {
            itemView.tvPostItemTitle.text = postVM.title
            itemView.cvPostItem.setOnClickListener {
                mListener?.onPostClicked(postVM.id ?: 1)
            }
        }
    }
}