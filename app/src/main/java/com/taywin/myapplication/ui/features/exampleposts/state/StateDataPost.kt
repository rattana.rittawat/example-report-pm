package com.taywin.myapplication.ui.features.exampleposts.state

import com.taywin.myapplication.datasource.model.PostResponseData

sealed class StateDataPost {
    class Post(val postVM: PostResponseData): StateDataPost()
    class Error(val message:String?): StateDataPost()
    class ShowLoader(val showLoader:Boolean): StateDataPost()
}