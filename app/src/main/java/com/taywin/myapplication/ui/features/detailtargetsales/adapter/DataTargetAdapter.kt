package com.taywin.myapplication.ui.features.detailtargetsales.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.taywin.myapplication.databinding.ItemListDetailReportTargetBinding
import com.taywin.myapplication.datasource.model.PostResponseData

class DataTargetAdapter : RecyclerView.Adapter<DataTargetAdapter.ViewHolder>() {

    private val mList: MutableList<PostResponseData> = mutableListOf()
    private var mListener: Listener? = null

    fun add(lstPostVM: PostResponseData) {
        mList.add(lstPostVM)
        notifyItemInserted(this.itemCount)
    }

    fun setListener(listener: Listener) {
        mListener = listener
    }

    interface Listener {
        fun onPostClicked(id: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ItemListDetailReportTargetBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun getItemCount() = mList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mList[position])
    }

    inner class ViewHolder(val binding: ItemListDetailReportTargetBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(postVM: PostResponseData) {
            binding.tvTargetSales.text = postVM.title
            binding.tvSales.text = postVM.title
            binding.tvSumOrDiv1.text = postVM.title
            binding.tvPercentageTarget.text = postVM.title
            binding.tvProfitTarget.text = postVM.title
            binding.tvProfitAchieved.text = postVM.title
            binding.tvSumOrDiv2.text = postVM.title
            binding.tvPercentageProfit.text = postVM.title

            binding.tvTargetSales.setOnClickListener {
                mListener?.onPostClicked(postVM.id ?: 1)
            }
        }
    }
}
