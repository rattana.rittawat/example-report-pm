package com.taywin.myapplication.ui.features.report

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.taywin.myapplication.R
import com.taywin.myapplication.databinding.SearchReportFragmentBinding
import com.taywin.myapplication.utils.MonthYearPickerDialog
import kotlinx.android.synthetic.main.search_report_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class SearchReportFragment : Fragment() {

    companion object {
        fun newInstance() = SearchReportFragment()
    }

    private val viewModel : SearchReportViewModel by viewModel()
    private var navController: NavController? = null
    private var _binding: SearchReportFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = SearchReportFragmentBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel

        viewModel.backwardEnable.observe(viewLifecycleOwner, Observer {
            binding.btnPrevMonthTarget.isEnabled = it
            binding.btnPrevMonthReport.isEnabled = it
            if (it) {
                binding.btnPrevMonthTarget.setImageResource(R.drawable.ic_arrow)
                binding.btnPrevMonthReport.setImageResource(R.drawable.ic_arrow)
            } else {
                binding.btnPrevMonthTarget.setImageResource(R.drawable.ic_arrow_disable)
                binding.btnPrevMonthReport.setImageResource(R.drawable.ic_arrow_disable)
            }
        })

        viewModel.forwardEnable.observe(viewLifecycleOwner, Observer {
            binding.btnNextMonthTarget.isEnabled = it
            binding.btnNextMonthReport.isEnabled = it
            if (it) {
                binding.btnNextMonthTarget.setImageResource(R.drawable.ic_arrow)
                binding.btnNextMonthReport.setImageResource(R.drawable.ic_arrow)
                binding.btnNextMonthTarget.rotation = 180f
                binding.btnNextMonthReport.rotation = 180f
            } else {
                binding.btnNextMonthTarget.setImageResource(R.drawable.ic_arrow_disable)
                binding.btnNextMonthReport.setImageResource(R.drawable.ic_arrow_disable)
                binding.btnNextMonthTarget.rotation = 180f
                binding.btnNextMonthReport.rotation = 180f
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = findNavController()
        binding.vm = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.setup()
        setOnClickUi()
    }

    private fun setOnClickUi() {
        btnSearchReport.setOnClickListener {
            navController?.navigate(SearchReportFragmentDirections.actionSearchReportFragmentToDetailReportTargetAllFragment())
        }
    }

}