package com.taywin.myapplication.ui.features.detailtargetsales

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewTreeViewModelStoreOwner
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.taywin.myapplication.R
import com.taywin.myapplication.ui.features.detailtargetsales.adapter.DataTargetAdapter
import com.taywin.myapplication.ui.features.exampleposts.MainAdapter
import com.taywin.myapplication.ui.features.exampleposts.MainViewModel
import com.taywin.myapplication.ui.features.exampleposts.state.StateDataPost
import kotlinx.android.synthetic.main.detail_report_target_all_fragment.*
import org.koin.android.ext.android.get

class DetailReportTargetAllFragment : Fragment(), MainAdapter.Listener {

    companion object {
        fun newInstance() = DetailReportTargetAllFragment()
    }

    private val viewModel: MainViewModel = get()
    private var mAdapter = MainAdapter()
    private var tAdapter = DataTargetAdapter()

    lateinit var scrollListenerMajor: RecyclerView.OnScrollListener
    lateinit var scrollListenerData: RecyclerView.OnScrollListener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.detail_report_target_all_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        mAdapter.setListener(this)

        rvPost.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = mAdapter
        }

        rvDataTarget.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = tAdapter
        }

        scrollListenerMajor = object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                rvDataTarget.removeOnScrollListener(scrollListenerData)
                rvDataTarget.scrollBy(dx, dy)
                rvDataTarget.addOnScrollListener(scrollListenerData)
            }
        }

        scrollListenerData = object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                rvPost.removeOnScrollListener(scrollListenerMajor)
                rvPost.scrollBy(dx, dy)
                rvPost.addOnScrollListener(scrollListenerMajor)
            }
        }

        rvPost.addOnScrollListener(scrollListenerMajor)
        rvDataTarget.addOnScrollListener(scrollListenerData)

        viewModel.getPosts()
    }

    private fun observeViewModel() {
//        TODO("Not yet implemented")
        viewModel.viewState.observe(viewLifecycleOwner, {
            when (it) {
                is StateDataPost.Post -> {
                    mAdapter.add(it.postVM)
                    tAdapter.add(it.postVM)
                }
                is StateDataPost.ShowLoader ->{
                    if(it.showLoader){
                        pbPosts.visibility = View.VISIBLE
                        rvPost.visibility = View.GONE
                        rvDataTarget.visibility = View.GONE
                    }else{
                        pbPosts.visibility = View.GONE
                        rvPost.visibility = View.VISIBLE
                        rvDataTarget.visibility = View.VISIBLE
                    }
                }
                is StateDataPost.Error -> {
                    Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                }
            }

        })
    }

    override fun onPostClicked(id: Int) {
//        TODO("Not yet implemented")
    }

}